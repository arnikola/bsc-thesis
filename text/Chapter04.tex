\chapter{Relation between the proton fraction at $\beta$-equilibrium and the symmetry energy} \label{Appendix A}
Here, the relationship that was presented in eq. \ref{2.19} is derived in detail. This relation declares that the proton fraction at $\beta$-equilibrium and zero temperature is uniquely specified by specifying the symmetry energy.

For cold $n-p-e$ matter the total energy per baryon can be expressed as the sum of the energies per baryon of the respective constituents:
\begin{equation} \label{A1}
E_{\mathrm{tot}}\left(n, Y_{p}\right)=E_{n}\left(n, Y_{n}\right)+E_{p}\left(n, Y_{p}\right)+E_{e}\left(n, Y_{e}\right)
\end{equation}
where we define $E_n$ as the energy per baryon of neutrons, $E_p$ as the energy per baryon of protons, and $E_e$ is the energy per baryon of electrons. These energies have all been expressed as a function of the number density, and the respective particle fractions: $Y_n$ is the neutron fraction, and $Y_e$ is the electron fraction. Of course, since in this thesis charge neutrality is imposed on the system, $Y_e=Y_p$.

We differentiate the above expression with respect to the proton fraction and get
\begin{equation} \label{A2}
\frac{\partial E_{\mathrm{tot}}\left(n, Y_{p}\right)}{\partial Y_{p}}=\frac{\partial E_{n}}{\partial Y_{n}} \frac{\partial Y_{n}}{\partial Y_{p}}+\frac{\partial E_{p}}{\partial Y_{p}}+\frac{\partial E_{e}}{\partial Y_{e}} \frac{\partial Y_{e}}{\partial Y_{p}}.
\end{equation}
Since the chemical potential of a species $q$ is given by $\mu_{q} \equiv \partial E_{q} /\left.\partial Y_{q}\right|_{S, n}$, eq. \ref{A1} is written as
\begin{equation} \label{A3}
\frac{\partial E_{\mathrm{tot}}\left(n, Y_{p}\right)}{\partial Y_{p}}=-\mu_{n}+\mu_{p}+\mu_{e},
\end{equation}
which when equal to zero is the condition for $\beta$-equilibrium. The total energy can be also deconstructed with regards to the nuclear symmetric matter and then add electrons,
\begin{equation} \label{A4}
E_{\mathrm{tot}}\left(n, Y_{p}\right)=E_{\mathrm{nucl}}(n, 1 / 2)+E_{\mathrm{sym}}(n)\left(1-2 Y_{p}\right)^{2}+E_{e}\left(n, Y_{e}\right).
\end{equation}
Differentiating eq. \ref{A4} yields
\begin{equation} \label{A5}
\frac{\partial E_{\text {tot }}\left(n, Y_{p}\right)}{\partial Y_{p}}=-4\left(1-2 Y_{p}\right) E_{\text {sym }}(n)+\frac{\partial E_{e}}{\partial Y_{e}} \frac{\partial Y_{e}}{\partial Y_{p}}.
\end{equation}
Of course since we have demanded charge neutrality and from the definition of the chemical potential, $\frac{\partial E_{e}}{\partial Y_{e}} \frac{\partial Y_{e}}{\partial Y_{p}}=\mu_e$. As such by combining eqs. \ref{A3} and \ref{A5} at $\beta$-equilibrium: 
\begin{equation} \label{A6}
\mu_{e}=4\left(1-2 Y_{p, \beta}\right) E_{\mathrm{sym}}(n)
\end{equation}
The chemical potential of relativistic electrons is defined,
\begin{equation} \label{A7}
\mu_{e}=\sqrt{p_{f}^{2} c^{2}+m_{e}^{2} c^{4}} \approx p_{f} c
\end{equation}
where the Fermi momentum of electrons is $p_{f} c=\left(3 \pi^{2} Y_{e} n\right)^{1 / 3} \hbar c$. The combinations of these expressions for the chemical potential we get
\begin{equation} \label{A8}
\left(3 \pi^{2} Y_{p, \beta} n\right)^{1 / 3} \hbar c=4\left(1-2 Y_{p, \beta}\right) E_{\mathrm{sym}}(n)
\end{equation}
or after we rewrite it,
\begin{equation} \label{A9}
\frac{Y_{p, \beta}}{\left(1-2 Y_{p, \beta}\right)^{3}}=\frac{64 E_{\mathrm{sym}}(n)^{3}}{3 \pi^{2} n(\hbar c)^{3}}
\end{equation}

In conclusion, from the above expression, if the proton fraction in $\beta$-equilibrium is known from any cold EOS of $n-p-e$ matter, it can be used to fit for the parameters of a specific model of the symmetry energy, $E_{sym}(n)$. In this framework, when one specifies the parameters $Y_{p,\beta}$, $S_0$, and $L$ they can use eq. \ref{A9} in order to fit for the parameter $\gamma$.