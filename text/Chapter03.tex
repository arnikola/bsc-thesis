\chapter{Results} \label{Chapter03}
In this chapter the complete expressions for the pressure of $n-p-e$ matter are given. We then proceed on showcasing our results regarding the performance of the model in extrapolating matter away from $\beta$-equilibrium and to arbitrary temperature.

\section{The complete pressure equations}
Using the energy relations derived in the previous chapter and the eq. \ref{2.12}, the expression for the pressure is obtained.

The pressure of $n-p-e$ matter is given by
\begin{equation} \label{3.1}
\begin{aligned}
P\left(n, Y_{p}, T\right)=&(\text { Cold EOS in } \beta \text { -equilibrium })+K\left(Y_{p}^{4 / 3}-Y_{p, \beta}^{4 / 3}\right) n^{4 / 3} \\
+& P_{\text {sym }}(n, T=0)\left[\left(1-2 Y_{p}\right)^{2}-\left(1-2 Y_{p, \beta}\right)^{2}\right] \\
+& \left\{\begin{array}{ll}
4 \sigma f_{s} T^{4} /(3 c), & n<n_{1} \\
n k_{B} T, & n_{1}<n<n_{2} \\
-\left[\frac{\partial a\left(0.5 n, 0.5 M_{\mathrm{SM}}\right)}{\partial n}+\frac{\partial a\left(Y_{p} n, m_{e}\right)}{\partial n} Y_{p}\right] n^{2} T^{2}, & n>n_{2}
\end{array}\right.
\end{aligned}
\end{equation}
where $n_1$ and $n_2$ are the transition densities for the thermal pressure at a particular temperature and proton fraction. The symmetry pressure is written as
\begin{equation*}
    P_{\text {sym }}(n, T=0)=\frac{2 \eta}{3} n E_{\text {sym }}^{\mathrm{kin}}(n)+\left[S_{0}-\eta E_{\text {sym }}^{\mathrm{kin}}\left(n_{\text {sat }}\right)\right]\left(\frac{n}{n_{\text {mt }}}\right)^{\gamma} \gamma n.
\end{equation*}
The expression for the proton fraction at $\beta$-equilibrium is given in \ref{2.19}. For an analytic derivation of said expression, one should see Appendix \ref{Appendix A}.
The derivatives for the effective mass are
\begin{equation*}
    \begin{array}{l}
\left.\frac{\partial a\left(n_{q}, M^{*}\right)}{\partial n}\right|_{Y_{q}}=-\left(\frac{2 a\left(n_{q}, M^{*}\right)}{3 n}\right) \times \\
\left\{1-\frac{1}{2}\left[\frac{M^{*}\left(n_{q}\right)^{2}}{M^{*}\left(n_{q}\right)^{2}+\left(3 \pi^{2} n_{q}\right)^{2 / 3}(h c)^{2}}\right]\left(\frac{\left(3 \pi^{2} n_{q}\right)^{2 / 3}(h c)^{2}}{M^{*}\left(n_{q}\right)^{2}}+\left.3 \frac{\left.\partial \ln \mid M^{*}\left(n_{q}\right)\right]}{\partial \ln n}\right|_{Y_{4}}\right)\right\},
\end{array}
\end{equation*}
and
\begin{equation*}
    \left.\frac{\partial \ln \left[M^{*}\left(n_{q}\right)\right]}{\partial \ln n}\right|_{Y_{q}}=-\alpha\left[1-\left(\frac{M^{*}\left(n_{q}\right)}{Y_{q} m c^{2}}\right)^{2}\right],
\end{equation*}
where, for symmetric matter,  one should replace $M^{*}\left(n_{q}\right) \rightarrow 0.5 M_{\mathrm{SM}}^{*}(0.5 n)$ and for electrons, $M^{*}\left(n_{q}\right) \rightarrow m_e$.
There are five parameters, $S_0$, $L$, $\gamma$, $n_0$, and $\alpha$ that are free. One can specify the values of $S_0$, $L$, and $Y_{p,\beta}$ and then fit for $\gamma$. Else, one can specify the values for $S_0$, $L$, and $\gamma$ and thus uniquely specify $Y_{p,\beta}$. The fits for $\gamma$, $n_0$, and $\alpha$ for the EOS in our sample are shown in Tables \ref{tab gamma fits} and \ref{tab alpha fits}.

Of course, as mentioned before, the piecewise expresssions for the thermal pressure are not helpful when running simulations because of the discontinuities they produce. For this reason the smoothed approximation is used, in the following form,
\begin{equation} \label{3.2}
P_{\mathrm{th}}\left(n, Y_{p}, T\right) \approx P_{\mathrm{rel}}+\left(P_{\mathrm{ideal}}^{-1}+P_{\mathrm{deg}}^{-1}\right)^{-1}.
\end{equation}
All the figures that show the thermal pressure in this thesis have been obtained using the smoothed approximation.

\section{Comparison of realistic EOS at arbitrary $Y_p$ and $T$}
In Chapter \ref{Chapter02} we showed that this new framework is capable of extrapolating matter from $\beta$-equilibrium to arbitrary proton fraction with small errors of $\lesssim 10\%$ at densities above $0.5\; n_{sat}$. In the same chapter we tested the model's ability to recreate the realistic data of the EOS for the thermal pressure. We found that the $M^{*}$ approximation can reproduce the thermal pressure data to errors within $\sim 10\%$. Here, we test the complete model's performance by taking a cold EOS from our sample and then subsequently extrapolating it to arbitrary proton fraction and temperature. The red and purple dots in Fig. \ref{fig full model FSG and DD2 0.1} show the predictions of FSUGold and DD2 while the solid lines the results of the model's approximation. We find almost perfect agreement between the data from FSUGold and DD2 and the approximation at $n\gtrsim 0.5\; n_{sat}$.

Figure \ref{fig full model FSG and DD2 0.1} shows the complete model performance for FSUGold and DD2 at six different temperatures and $Y_p=0.1$. The model begins with the relevant cold EOS in $\beta$-equilibrium and then adds the appropriate corrections to extrapolate to $Y_p=0.1$ and the six temperatures. The values for parameters $\gamma$, $n_0$, and $\alpha$ and taken from Tables \ref{tab gamma fits} and \ref{tab alpha fits} for each EOS.
\begin{figure}[h]
    \centering
    \includegraphics[width=10cm, height=12cm]{Full Model FSG, DD2 at 6 Temperatures Yp 0.1.png}
    \caption{The pressure derived by the model and the realistic data from FSUGold and DD2. The data are shown with red and purple dots while the approximation in solid similarly coloured lines. The six graphs are at $Y_p=0.1$ and $k_BT= 1$, $10$, $15$, $20$, $30$, and $47.9\; \mathrm{MeV}$, respectively. We find that the model accurately recreates the EOS data from FSUGold and DD2 at densities above $n_{sat}$ at all temperatures, while it produces small errors at smaller densities}
    \label{fig full model FSG and DD2 0.1}
\end{figure}

In Fig. \ref{fig full model sample 0.05} we plot the residuals between the model and the realistic EOS for FSUGold, DD2, as well as the rest EOS in our sample at $Y_p=0.05$. The values for $\gamma$, $n_0$, and $\alpha$ for each EOS are taken from Tables \ref{tab gamma fits} and \ref{tab alpha fits}. We find that the approximation closely recreates the EOS data in our sample. Specifically, for $n\geq n_{sat}$ the errors produced are less than $20\%$ at all six temperatures. We also note that the errors introduced by the approximation for all the EOS increases at $\sim0.5n_{sat}$. 

Similarly, in Figs. \ref{fig full model sample 0.05}, \ref{fig full model sample 0.1}, \ref{fig full model sample 0.3}, and \ref{fig full model sample 0.4}, the corresponding residuals between the data and the model are shown for six temperatures and $Y_p=0.05$, $Y_p=0.1$, $0.3$, and $0.4$, respectively.

We have, therefore, confirmed that the \cite{raithel} model based on a simple set of parameters, can reproduce the data from the realistic EOS from our sample. As such one can now use this framework in two ways. Firstly, the approximation can be used to calculate EOS data, in order to avoid complicated calculations. Secondly, one can use this model to create a new finite-temperature EOS for $n-p-e$ matter. Through the specification of the values of parameters $S_0$, $L$, $\gamma$, $n_0$, and $\alpha$ one can, in principle, create a finite-temperature EOS that will probe new physics. In conclusion, the \cite{raithel} model has a diverse range of applications through its simplicity of calculations and simple set of physically motivated parameters. 

\begin{figure}[h]
    \centering
    \includegraphics[width=14cm, height=16cm]{Full Model Sample at 6 Temperatures Yp 0.05.png}
    \caption{The residuals between the model and the realistic data from all EOS in our sample. The six panels are at $Y_p=0.05$ and $k_BT=1,\;10,\;15,\;20,\;30$ and $47.9\; \mathrm{MeV}$, respectively.}
    \label{fig full model sample 0.05}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=14cm, height=16cm]{Full Model Sample at 6 Temperatures Yp 0.1.png}
    \caption{The residuals between the model and the realistic data from all EOS in our sample. The six panels are at $Y_p=0.1$ and $k_BT=1,\;10,\;15,\;20,\;30$ and $47.9\; \mathrm{MeV}$, respectively.}
    \label{fig full model sample 0.1}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=14cm, height=16cm]{Full Model Sample at 6 Temperatures Yp 0.3.png}
    \caption{The residuals between the model and the realistic data from all EOS in our sample. The six panels are at $Y_p=0.3$ and $k_BT=1,\;10,\;15,\;20,\;30$ and $47.9\; \mathrm{MeV}$, respectively.}
    \label{fig full model sample 0.3}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=14cm, height=16cm]{Full Model Sample at 6 Temperatures Yp 0.4.png}
    \caption{The residuals between the model and the realistic data from all EOS in our sample. The six panels are at $Y_p=0.4$ and $k_BT=1,\;10,\;15,\;20,\;30$ and $47.9\; \mathrm{MeV}$, respectively.}
    \label{fig full model sample 0.4}
\end{figure}