\chapter{The Neutron Star EOS} \label{Chapter01}
We begin this chapter with a general discussion on how neutron stars are formed. Information and equations in this first section come from the PhD Thesis of \cite{fortin}, unless specified otherwise. The reader is encouraged to look into the relevant chapter's of the dissertation for a more in depth discussion on the subject. 

\section{Birth of Neutron Stars}
Stars the are of high mass evolve much faster than their lower mass counterparts. This happens since the phases of their stellar evolution are sped up due to their large mass. High mass stars fuse heavier elements than hydrogen and helium up to iron via a nucleosynthesis chain. At the end of their life, they are observed as red supergiants with large radii and low surface temperatures. If we were to cut open such an object to study its interior we would find that it is organised in layers consisting of heavier and heavier elements as we move towards the core. 

The core of the red supergiant now consists of iron and other iron rich nuclei. When the mass of the core reaches the Chandrasekhar limit of $1.2\;M_\odot$, the collapse of the core starts and the star implodes. Gravity overtakes the pressure of the gas which cannot further support the equilibrium of the star, and the star falls on itself. The outer layers of the star bounce on the collapsed core and are subsequently cast off in a very energetic explosion.

The mechanism described above is the type II supernova. The remnant of the explosion that is left behind can either be a neutron star or a black hole. We will focus on a neutron star.

Type II supernova are some of the events that yield the highest energies in our Universe. If one subtracts the binding energy of the proto neutron star created after the explosion, from the binding energy of the iron core of the star before the explosion, using typical values for the mass and radius of the neutron star (i.e. $M=1.4\;M_\odot$ and $R=10\;\mathrm{km}$), they will get

\begin{equation*}
\begin{aligned}
E_{\mathrm{SNII}} & \sim E_{\mathrm{bind, pre\;SN}}-\mathrm{E}_{\mathrm{bind, proto\;NS}}, \\
& \sim -\frac{3}{5} \frac{GM_{\mathrm{Fe}}^{2}}{R_{\mathrm{Fe}}} + \frac{3}{5} \frac{GM^{2}}{R}, \\
& \sim 3 \times 10^{53}\;\mathrm{erg}.
\end{aligned}
\end{equation*}
This value is the same amount of energy our Sun produces during ten billion years.

The neutron stars with masses and radii of the typical proportions mentioned above, yield a compactness parameter
\begin{equation*}
\frac{G M}{c^{2} R} \sim 0.2.
\end{equation*}
As such they are objects of extreme compactness and hence, relativistic. Central densities of neutron stars are $10^{14}$ times more dense than matter found on Earth. For this reason the state of matter in neutron stars remains an open question to this day. Both Astrophysics and Nuclear Physics have made one of their goals to describe and constrain the so called EOS.

\section{Neutron Star Structure}

\subsection{The TOV equations}
The EOS is essentially a relation between thermodynamic variables in a system. Most of the time, when the term EOS appears in literature, it is used for the pressure-energy relation, $P=P(\epsilon)$. As a relation of thermodynamic variables, the EOS expresses the behaviour of the microphysics in a system. This description of the microphysics given by the EOS can then be connected to the macroscopic properties of neutron stars using the stellar structure equations. Since, neutron stars are objects of extreme compactness the classical equations fail. The answer to this problem was given by Tolman and Oppenheimer and Volkoff, who concurrently and independently derived a relativistic version of the stellar structure equations, the so-called "TOV" equations. The TOV equations were derived by considering a perfect fluid in static, spherically symmetric equilibrium and solving the Einstein equations for such a configuration. They are given as

\begin{equation}
\frac{d P}{d r}=-\frac{(\epsilon+P)\left(m+4 \pi r^{3} P\right)}{r(r-2 m)}, \label{1.1}
\end{equation}
\begin{equation}
\frac{d m}{d r}=4 \pi r^{2} \epsilon \label{1.2}
\end{equation}
where $P$ is the pressure, $\epsilon$ is the energy density, $m$ is the mass and $G$ and $c$, the gravitational constant and speed of light, respectively are set to 1. To complete this set of equations one needs to specify the metric of the spacetime, and the pressure-energy relation (the EOS). For a spherically symmetric, stationary and asymptotic spacetime in Schwarzshild coordinates $t,r, \theta, \phi$: $d s^{2}=-e^{\nu} d t^{2}+e^{\lambda} d r^{2}+r^{2}\left(d \theta^{2}+\sin ^{2} \theta d \phi^{2}\right)$. So the remaining equations are written as
\begin{equation*} 
    \begin{array}{c}
P=P(\epsilon) \\
\frac{d \nu}{d r}=-\frac{2}{\epsilon+P} \frac{d P}{d r}=\frac{2\left(m+4 \pi r^{3} P\right)}{r(r-2 m)} \\
\lambda=-\ln \left(1-\frac{2 m}{r}\right)
\end{array}
\end{equation*}

The importance of these equations becomes obvious if we consider that they provide a direct relation between the pressure and energy density of an EOS and the stellar mass enclosed in a given radius. Once $P=P(\epsilon)$ is specified, one has to integrate the TOV equations from the center $r=0$ where $P=P_c$ and $\epsilon=\epsilon_c$, to the surface at $r=R$, where $P=0$ and the enclosed mass is equal to the total mass, $m=M$. Arbitrary central value can be given to $\nu$, say $\nu(0)=-1$. In other words, by integrating the TOV equations for each EOS the nuclear physics as described by the $P=P(\epsilon)$ relation is translated to the mass-radius relation and by extent to characteristics of neutron stars that are measured through astrophysical observations.

\subsection{Matter inside the neutron star}

Before we go into discussing the various ideas put forward by authors in regards to calculating the cold dense matter EOS, it would be useful to go into some detail about why the state of matter in a neutron star does not warranty a simple and direct answer. The cores of neutron stars are consisted of matter that is cold and ultradense, accompanied by a big asymmetry in the numbers of protons and neutrons and large chemical potentials. Indeed, sufficiently cold neutron stars have reached $\beta$-equilibrium, where the rate at which beta decay and inverse beta decay happen are the same. The condition for $\beta$-equilibrium, given as
\begin{equation}
\left(\frac{\partial \epsilon / n}{\partial Y_{e}}\right)_{n}=\mu_{e}+\mu_{p}-\mu_{n}=0 \label{1.3}
\end{equation}
points to proton fractions $Y_p=n_p / n \lesssim 10\%$, where $n_p$ is the proton density and $n$ is the overall baryon density. This is unlike normal nuclei where the numbers of protons and neutrons are equal. Such matter is called symmetric matter. In equation (\ref{1.3}), $\epsilon/n$ is the energy per baryon, $\mu_i$ are the chemical potentials for each species, and $Y_e$ is the number of electrons per baryon. Neutron star core densities can go up to $\sim 10\;\rho_{sat}$, where $\rho_{sat}\simeq 2.8 \times 10^{14}\;\mathrm{g}\mathrm{cm}^{-3}$ is the nuclear saturation density expressed as mass density. The nuclear saturation density is the density at which the distance between particles becomes so small that they are very close to the distance at which nuclear forces are repulsive. It can be equivalently expressed as a particle density, $n_{sat}\simeq 0.16\;\mathrm{fm}^{-3}$. Considering that neutrons overlap geometrically at $\sim 4\;\rho_{sat}$, in such high densities it is natural that matter may not be consisted by nucleons alone but may contain a complex variety of hadronic degrees of freedom. Furthermore, when the overlap between nucleons is greater, transitions to non-nucleonic states of matter can be expected wherever it is energetically favoured. One very likely scenario is the progressive appearance of quark degrees of freedom and quark constituents, resulting in a system that consists of a nucleon crust and a quark core. It is also possible, due to the weak interactions in the cold neutron star cores, for matter to be of state with a high degree of strangeness. Such models include the presence of hyperons, stars with free quarks and self-bound stars consisting of strange matter.

\subsection{Calculating the cold EOS}

Many calculations have been made to date using first principle QCD. However, because they describe quark matter interactions at several times the saturation density, they have not been able to be extrapolated down to low densities. For this reason, a slew of different approaches have been followed in calculating and modelling the equation of state of neutron star matter.

One of the more common methods is based on assuming that the forces between particles are described by static few-body potentials. Nuclear Hamiltonians are then expanded into two-, three-,... n-body static potential terms. Here the two-body potential is determined in the vicinity of $n_{sat}$ for symmetric matter using nucleon-nucleon scattering data below $350\;\mathrm{MeV}$ and the properties of light nuclei, and the contributions from the three-body potentials are then added. The EOS that results from such calculations have been found to have sensitivity to the three-nucleon interactions. This happens because the kinetic term and the two-body term in such potentials cancel each other out almost completely. Furthermore, the importance of three and higher body forces has a direct dependence on the density and, thus, such methods break down at $n \gg n_{sat}$.

In these approaches followed when calculating an EOS for matter with unequal numbers of protons and neutrons, one common formalism is to deconstruct the EOS in two parts. A symmetric matter part and a second part. This expansion of the EOS happens with respect to the asymmetry parameter, $(1-2Y_p)$. This can be generally written as (see Chapter 6 from the PhD dissertation of \cite{raithephd} for more information)
\begin{equation}
E_{b}\left(n, Y_{p}\right)=E_{0}(n)+E_{\mathrm{sym}}(n)\left(1-2 Y_{p}\right)^{2}, \label{1.4}
\end{equation}
where $E_b(n,Y_p)$ symbolizes the energy per baryon as a function of the baryon density $n$ and proton fraction $Y_p$, $E_0(n)$ is the energy of symmetric matter and $E_{sym}(n)$ is the so called symmetry energy. 

The symmetry energy is defined as the difference in energy between symmetric matter and matter with unequal numbers of protons and neutrons. It is customary to further expand the symmetry energy about the density. Such expansions show that the symmetry energy is expressed in terms of two main parameters. These are the the symmetry energy at nuclear saturation density, $S_0$, and the parameter $L$, related to the slope of the symmetry energy. The symmetry energy is an essential term not only for nuclear physics, but for astrophysics as well. Astrophysical observations on the mass of neutron stars are indirectly testing the symmetry energy at high densities. The importance of the value of the symmetry energy is big because it plays a key role in determining the neutron star radius, in affecting the gravitational wave emission during neutron star mergers, and the outcomes of CCSN. Nevertheless, the symmetry energy and its parameters are sufficiently constrained by both nuclear experiments and astrophysical observations. 

Many alternatives to this approach have been suggested: theoretical calculations of constituents interacting via meson exchange, and ab initio calculations based on non-relativistic, relativistic models and models modified with Skyrme forces. Either way, it is obvious that a wide net has been cast by the various candidate theories proposed to describe the EOS. One natural follow up question to this would be how do we disentagle all these approaches and then choose one?

\section{Constraints on the EOS}
The most safe and secure way to go through all the ideas put forward about how matter behaves at the extreme conditions imposed in a neutron star is through data. As mentioned in the section above, integrating the TOV equations results in the mass-radius relation for a given EOS (see Fig. \ref{fig mass-radius} for an up to date plot of the mass radius relation for a number of EOS). It turns out that the mass of the neutron star has a maximum value as a function of radius (or central density), above which the star is unstable against collapsing to a black hole. The value of the maximum mass is the first and most concrete constraint on the EOS, since it depends on the EOS itself. This means that the observation of a mass higher than the maximum one allowed by a given EOS simply rules out that EOS. Up to now the best microscopic EOS are compatible with the largest observed masses, close to $\sim 2 \; M_{\odot}$. One desire is to constrain the EOS through phenomenological data on the radius of neutron stars.

\begin{figure}[h]
    \centering
    \includegraphics[width=14cm, height=8cm]{Mass-Radius.png}
    \caption{A large sample of proposed EOS calculated under different physical theories is presented in the left. The mass-radius curves corresponding to the EOS are shown in the right panel. The figure is taken from \cite{ozelfreire}}
    \label{fig mass-radius}
\end{figure}

The unique map that the TOV equations provide between the EOS and the macroscopic properties of the stars, and in particular, their masses and radii, can be exploited to infer the EOS from astrophysical measurements. However, in order to invert this mapping a measurement of the entire mass-radius curve is required. This means that the radii for neutron stars that span the range of stellar masses between $\sim0.2-2\; M_{\odot}$ is required. Since neutron stars with masses much smaller than the Chandrasekhar mass are not formed and neither observed, the application of this inversion of the mapping is greatly limited.

Despite this snag, the connection between the mass-radius relations and the EOS themselves, turns out to have certain characteristics that makes it able for them to be reconstructed through a much smaller sample of neutron star radii. In fact it has been suggested that only a small number of densities determines the macroscopic characteristics of neutron stars. Specifically, the maximum mass of a mass-radius relation is determined primarily by the behaviour of the EOS at high densities ($P_1$). The radius depends primarily on the pressure at $\sim2\; \rho_{sat}$ ($P_2$). Lastly, the slope of the mass-radius relation, and as such, whether the radius increases or decreases with the mass, depends almost solely on the pressure at $\sim 4\; \rho_{sat}$ ($P_3$).

The existence of such connections allows for the pressure of matter to be inferred at a few key densities instead of mapping the whole functional form of the EOS from astrophysical measurements. Studies have been made in order to determine the key densities for sampling that lead to small uncertainties in determining the EOS. The specification of the values $P_1$, $P_2$, and $P_3$, and the subsequent connection of those with piecewise polynomials has lead to the recreation of more than forty EOS (\cite{read}, \cite{ozelpsaltis}) with small errors. Thus the measurement of the masses and radii of a small number of neutron stars can, in fact, give us information on the microphysics calculations. 

As discussed previously in this chapter, the mass of the heaviest neutron star measured places a very concrete constraint on the EOS. Nevertheless, there exist other constraints that are of more strict nature. One of such constraints is achieved by putting together the smallest of the maximum mass and radius measurements. Since EOS that lead to smaller radii generally have smaller maximum masses, such measurements can be used to confine the parameter range in the EOS.

Even more techniques have been developed, most of them to combine radius measurements, the maximum mass constraint, as well as data from low energy experiments. One such technique used is the Bayesian inference. It is based on Bayes' theorem, which uses prior knowledge of conditions related to an event, to describe the probability of an event. Thus, they calculate the likelihoods over the EOS parameters $P_1$, $P_2$, and $P_3$, from the likelihoods of the mass-radius of the sources.

The priors for the values of $P_1$, $P_2$, and $P_3$, are specified through various constraints. On a basis level, it is required that $P_3\geqslant P_2\geqslant P_1$, and also that $P_1$ is greater or equal to the pressure of matter at $\rho_0=10^{14}\;\mathrm{g\;cm^{-3}}$. Next, the causality condition is imposed at all densities,
\begin{equation*}
c_{s}^{2}=\frac{\partial P}{\partial \epsilon} \leq c^{2},
\end{equation*}
where $c_s$ is the sound speed. Lastly, to fold the maximum mass requirement in, the maximum mass of each EOS corresponding to a $P_i$ triplet needs to exceed $1.97M_{\odot}$, within $1\sigma$ with the heaviest neutron star observed.