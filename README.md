# BSc Thesis

Here, my Diploma in Physics Thesis content is stored in three appropriate files.


In the "code" file you will find a Jupyter notebook in which I have rewritten my code in such a form that it can be run by someone who does not have a particularly deep knowledge of the subject. Of course, the code is based on the model published by Raithel et al. (2019) and the reader is encouraged to go through that publication first. Some EOS files (specifically for the FSG EOS) are offered along with the code in order for the user to succesfully run the notebook.

What this code can essentially do is, through performing several calculations, extend any Cold Beta-equilibriated EOS to an arbitrary proton fraction and temperature. Then it stores the results in an appropriately named .txt file for further use, depending on the user's wishes.


Finally, my Thesis text and presentation can be found in the remaining two files.
